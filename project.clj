(defproject core "0.1.0-SNAPSHOT"
  :description "Kebuu Core Business Logic"
  :url "http://"
  :license {:name "The MIT License (MIT)"
            :url "https://bitbucket.org/kebuu/core/src/LICENSE"}
  :dependencies [[org.clojure/clojure "1.5.1"]])
